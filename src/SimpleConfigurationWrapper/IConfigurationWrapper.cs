﻿using System.Collections.Generic;
using System.Runtime.InteropServices.ComTypes;
using SimpleConfigurationWrapper.Entries;

namespace SimpleConfigurationWrapper
{
    /// <summary>
    /// 
    /// </summary>
    public interface IConfigurationWrapper
    {
        /// <summary>
        /// Returns a validation result. Annotates validation result with exception if any occurred.
        /// </summary>
        /// <returns></returns>
        IEnumerable<ConfigurationEntryValidationResult> Validate(bool includeDefaultValueString = false);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<IConfigurationEntry> GetAllConfigurationEntries();
    }
}