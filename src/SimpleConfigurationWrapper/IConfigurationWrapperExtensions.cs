﻿using SimpleConfigurationWrapper.Providers;

namespace SimpleConfigurationWrapper
{
    /// <summary>
    /// 
    /// </summary>
    public static class IConfigurationWrapperExtensions
    {
        public static T WithValueProvider<T>(this T wrapper, IConfigValueFacade configValueFacade) where T : IConfigurationWrapper
        {
            var allEntries = wrapper.GetAllConfigurationEntries();
            foreach (var entry in allEntries)
            {
                entry.SetConfigValueFacade(configValueFacade);
            }
            return wrapper; 
        }
    }
}