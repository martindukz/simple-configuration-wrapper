﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleConfigurationWrapper.Extensions;

namespace SimpleConfigurationWrapper.Util
{
    /// <summary>
    /// Utility class for generating the configuration skeleton for a given configuration.
    /// </summary>
    public class GenerateDefaultConfiguration
    {
        /// <summary>
        /// Generate a 
        /// </summary>
        /// <param name="simpleConfig"></param>
        /// <returns></returns>
        public string GenerateForAppSettings(IConfigurationWrapper simpleConfig)
        {
            var validationResults = simpleConfig.Validate(true);
            var result = new StringBuilder(); 
            foreach (var validationResult in validationResults)
            {
                //Breaks if description or examples contain --> 
                result.Append("<!-- Config entry info: : " + validationResult.Description);
                if (!validationResult.Examples.IsNothing())
                {
                    result.Append(Environment.NewLine + "         Examples: " + validationResult.Examples);
                }
                result.Append("  -->" + Environment.NewLine);

                result.Append("<add key=\"" + validationResult.Identifier + "\" value=\"" + validationResult.DefaultValueString + "\"/>");
                result.Append(Environment.NewLine);
            }


            return result.ToString();
        }
    }
}
