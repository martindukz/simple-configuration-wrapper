﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace SimpleConfigurationWrapper.Providers
{

    public class ConfigurationManagerValueFacade : IStringConfigValueFacade
    {
        public bool GetValue(string identifier, out string result)
        {
            if (!ConfigurationManager.AppSettings.AllKeys.Contains(identifier))
            {
                result = null; 
                return false; 
            }
            result = ConfigurationManager.AppSettings[identifier];
            return true; 
        }

        public string GetValue(string identifier, bool throwIfNotFound = false)
        {
            if (!ConfigurationManager.AppSettings.AllKeys.Contains(identifier))
            {
                if (throwIfNotFound)
                {
                    throw new KeyNotFoundException("Did not find key: " + identifier);
                }
                return null; 
            }
            return ConfigurationManager.AppSettings[identifier];
        }
    }
}