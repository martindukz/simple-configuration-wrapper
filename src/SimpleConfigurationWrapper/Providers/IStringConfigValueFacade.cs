﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleConfigurationWrapper.Providers
{
    public interface IStringConfigValueFacade : IConfigValueFacade<string>
    {
     }

    public interface IConfigValueFacade<T> : IConfigValueFacade
    {
        bool GetValue(string identifier, out T result);
        T GetValue(string identifier, bool throwIfNotFound = false);
    }

    public interface IConfigValueFacade
    {
    }
}
