﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleConfigurationWrapper.Extensions
{
    public static class StringExtensions
    {
        public static bool IsNothing(this string s)
        {
            return string.IsNullOrWhiteSpace(s);
        }

        public static string Join(this IEnumerable<string> strings, char joiner)
        {
            return strings.Join(joiner.ToString()); 
        }

        public static string Join(this IEnumerable<string> strings, string joiner)
        {
            return String.Join(joiner, strings);
        }

        public static string[] SplitSimple(this string target, char seperator, StringSplitOptions split = StringSplitOptions.RemoveEmptyEntries)
        {
            return SplitSimple(target, seperator.ToString(), split);
        }

        public static string[] SplitSimple(this string target, string seperator, StringSplitOptions split = StringSplitOptions.RemoveEmptyEntries)
        {
            if (target.IsNothing())
            {
                return Array.Empty<string>();
            }
            return target.Split(new[] {seperator}, split);
        }
    }
}
