﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using SimpleConfigurationWrapper.Entries;
using SimpleConfigurationWrapper.Providers;

namespace SimpleConfigurationWrapper
{
    /// <summary>
    /// Wrapper for configurations. Acts as base class for configurationfiles.
    /// </summary>
    public class ConfigurationWrapper : IConfigurationWrapper
    {
  
        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static string TypePrefix<T>(string identifier) where T : IConfigurationWrapper
        {
            return typeof(T).FullName + "_" + identifier;
        }

        /// <summary>
        /// The ConfigValueFacade, to be used when creating configurationentries.
        /// </summary>

        
        /// <summary>
        /// The ConfigurationWrapper defaults to ConfigurationManagerValueFacade (app.config, web.config) if none provided.
        /// </summary>
        /// <param name="configValueFacade"></param>
        public ConfigurationWrapper()
        {
            
        }
      
        /// <summary>
        /// Validates all configuration entries. Result contains errors/exceptions if any. 
        /// </summary>
        /// <returns></returns>
        public IEnumerable<ConfigurationEntryValidationResult> Validate(bool includeDefaultValueString = false)
        {
            var validateThese = GetAllConfigurationEntries();

            var validationResults = validateThese.Select(configEntry => configEntry.Validate(includeDefaultValueString));

            return validationResults.ToList(); 
        }

        /// <summary>
        /// Get all the configuration entries for this configuration wrapper.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<IConfigurationEntry> GetAllConfigurationEntries()
        {
            var properties = GetType()
                .GetProperties(BindingFlags.GetProperty | BindingFlags.Public | BindingFlags.Instance);

            var configEntryProperties = properties.Where(prop => typeof(IConfigurationEntry).IsAssignableFrom(prop.PropertyType));
            var result = configEntryProperties.Select(configEntry => configEntry.GetValue(this))
                .Cast<IConfigurationEntry>();

            return result; 
        }
    }
}
