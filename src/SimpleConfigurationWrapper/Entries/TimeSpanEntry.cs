﻿using System;
using System.Configuration;
using System.Globalization;
using SimpleConfigurationWrapper.Providers;

namespace SimpleConfigurationWrapper.Entries
{
    /// <summary>
    /// 
    /// Uses by default the "c" format: https://docs.microsoft.com/en-us/dotnet/standard/base-types/standard-timespan-format-strings
    /// [-][d’.’]hh’:’mm’:’ss[‘.’fffffff]
    /// </summary>
    public class TimeSpanEntry : ConfigurationEntry<TimeSpan>
    {
        private readonly CultureInfo _cultureInfo;

        public TimeSpanEntry(IStringConfigValueFacade valueFacade, string identifier, TimeSpan defaultValue = default(TimeSpan), string description = null, string examples = null, CultureInfo cultureInfo = null) : base(valueFacade, identifier, defaultValue, description, examples)
        {
            _cultureInfo = cultureInfo ?? CultureInfo.InvariantCulture;
        }

        public TimeSpanEntry(string identifier, TimeSpan defaultValue = default(TimeSpan), string description = null, string examples = null, CultureInfo cultureInfo = null) : base(identifier, defaultValue, description, examples)
        {
            _cultureInfo = cultureInfo ?? CultureInfo.InvariantCulture;
        }


        public override TimeSpan ConvertFromString(string stringValue)
        {
            try
            {
                return TimeSpan.Parse(stringValue, _cultureInfo);
            }
            catch (Exception ex)
            {
                throw new ConfigurationErrorsException("Encountered exception parsing configuration value " + stringValue + " for TimeSpan With message " + ex.Message, ex);
            }
        }

        public override string ConvertToString(TimeSpan value)
        {
        
            return value.ToString("c", _cultureInfo);
        }
    }
}