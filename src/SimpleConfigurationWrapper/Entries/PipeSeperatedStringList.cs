﻿using System.Collections.Generic;
using System.Linq;
using SimpleConfigurationWrapper.Attributes;
using SimpleConfigurationWrapper.Providers;

namespace SimpleConfigurationWrapper.Entries
{
    /// <summary>
    /// Should I create a generic list? Maybe list of configuration entry. 
    /// </summary>
    [ConfigurationEntryFormatDocumentation("A simple string list configuration", "\"example\"|\"example2\"")]
    public class PipeSeperatedStringList : ConfigurationEntry<List<string>>
    {
        private readonly char _seperator;

        public PipeSeperatedStringList(IStringConfigValueFacade stringValueFacade, string identifier, List<string> defaultValue = null, char seperator = '|', string description = null, string examples = null) : base(stringValueFacade, identifier, defaultValue, description, examples)
        {
            _seperator = seperator;
        }


        public override List<string> ConvertFromString(string stringValue)
        {
            if (string.IsNullOrWhiteSpace(stringValue))
            {
                return new List<string>();
            }

            var result = stringValue.Split(_seperator).Select(s => s?.Trim()).ToList();

            return result; 
        }

        public override string ConvertToString(List<string> value)
        {
            if (value == null || value.Count == 0)
            {
                return ""; 
            }
            var result = string.Join("|", value);

            return result; 
        }
    }
}