﻿using System;
using System.Reflection;
using SimpleConfigurationWrapper.Attributes;

namespace SimpleConfigurationWrapper.Entries
{
    /// <summary>
    /// A validation result for configuration entry.
    /// </summary>
    public class ConfigurationEntryValidationResult
    {
        public string Identifier { get; }
        public object DefaultValue { get; }
        public string Description { get;  }
        public string Examples { get; }
        public bool ValidationFailed { get; set; }
        public Exception Exception { get; set; }
        public string ConfigEntryFormatDesc { get; private set; }
        public string ConfigEntryExamples { get; private set; }
        public string ConfigEntryValue { get; set; }
        public string DefaultValueString { get; set; }
        public string ValidationMessage { get; set; }

        public ConfigurationEntryValidationResult(string identifier, object defaultValue, string description, string examples)
        {
            Identifier = identifier;
            DefaultValue = defaultValue;
            Description = description;
            Examples = examples;
        }

        public void EnrichWithFormatInfo(Type configEntryType)
        {
            var format = configEntryType.GetCustomAttribute<ConfigurationEntryFormatDocumentationAttribute>(false);
            if (format == null)
                return;
            ConfigEntryFormatDesc = format.ConfigurationDescription;
            ConfigEntryExamples = format.ConfigurationDescription; 
        }
    }
}