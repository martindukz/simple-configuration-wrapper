﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleConfigurationWrapper.Attributes;
using SimpleConfigurationWrapper.Extensions;
using SimpleConfigurationWrapper.Providers;

namespace SimpleConfigurationWrapper.Entries
{
    [ConfigurationEntryFormatDocumentation("Use this for a configurable mapping from a resolvable basetype (e.g. interface) to a specific configured type.", new []{ "SimpleConfigurationWrapper.Examples.ExampleClasses.SomeImpl2, SimpleConfigurationWrapper.Examples", "SimpleConfigurationWrapper.Examples.ExampleClasses.SomeImpl2, SimpleConfigurationWrapper.Examples, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null" })]
    public class DepInjEntry<TSource> : ConfigurationEntry<Type>
    {
        public DepInjEntry(string identifier, Type defaultValue, string description = null, string examples = null) : base(identifier, defaultValue, description, examples)
        {

        }

        public DepInjEntry(IStringConfigValueFacade stringConfigStoreFacade, string identifier, Type defaultValue, string description = null, string examples = null) : base(stringConfigStoreFacade, identifier, defaultValue, description, examples)
        {

        }

        public Type SourceType 
        {
            get { return typeof (TSource); }
        }

        public override Type ConvertFromString(string stringValue)
        {
            //Allow null - but not faulty types. 
            if (stringValue.IsNothing())
                return null; 

            var result = Type.GetType(stringValue);
            if (result == null)
            {
                throw new ConfigurationErrorsException("Type not found: " + stringValue);
            }
            return result; 
        }

        public override string ConvertToString(Type value)
        {
            return value.AssemblyQualifiedName; 
        }
    }
}
