using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using SimpleConfigurationWrapper.Attributes;
using SimpleConfigurationWrapper.Extensions;
using SimpleConfigurationWrapper.Providers;

namespace SimpleConfigurationWrapper.Entries
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TKey"></typeparam>
    /// <typeparam name="TValue"></typeparam>
    [ConfigurationEntryFormatDocumentation("A simple dictionary TKey to TValue simple keys and values", "\"example\"|\"example2\"")]
    public class DictionaryEntry<TKey, TValue> : ConfigurationEntry<Dictionary<TKey, TValue>>
        where TKey : IConvertible
        where TValue : IConvertible
    {
        private readonly char _itemSeperator;
        private readonly char _kevValueSeperator;

        public DictionaryEntry(IStringConfigValueFacade stringValueFacade, string identifier, Dictionary<TKey, TValue> defaultValue = null, string description = null, string examples = null, char itemSeperator = ';', char kevValueSeperator = ':') : base(stringValueFacade, identifier, defaultValue, description, examples)
        {
            _itemSeperator = itemSeperator;
            _kevValueSeperator = kevValueSeperator;
        }

        public override Dictionary<TKey, TValue> ConvertFromString(string stringValue)
        {
            if (stringValue.IsNothing()) { 
                return new Dictionary<TKey, TValue>();
            }
            var items = stringValue.SplitSimple(_itemSeperator);

            var keyValuePairs = items.Select(item => item.SplitSimple(_kevValueSeperator, StringSplitOptions.None)).ToList();
            if (keyValuePairs.Any(kvp => kvp.FirstOrDefault().IsNothing()))
            {
                throw new ConfigurationErrorsException("Found null-key in keyvalue dictionary: " + stringValue);
            }
            var tooLongs = keyValuePairs.Where(kvp => kvp.Length > 2).ToList();
            
            if (tooLongs.Any())
            {
                throw new ConfigurationErrorsException("Faulty keyvalue pair. Had more than key and value: " + tooLongs.Select(tooLong => tooLong.Join(_kevValueSeperator)));
            }
            var result = new Dictionary<TKey, TValue>();
            foreach (var kvp in keyValuePairs)
            {
                var key = ConvertTo<TKey>(kvp.First().Trim());
                var value = ConvertTo<TValue>(kvp.Skip(1).FirstOrDefault()?.Trim());
                result.Add(key, value);
            }

            return result; 
        }

        private T ConvertTo<T>(string toConvert)
        {
            return (T) Convert.ChangeType(toConvert, typeof (T)); 
        }

        public override string ConvertToString(Dictionary<TKey, TValue> value)
        {
            if (value == null)
                return null;

            var result = value.Select(kvp => kvp.Key.ToString() + _kevValueSeperator + kvp.Value.ToString()).Join(_itemSeperator);

            return result;
        }
    }


}