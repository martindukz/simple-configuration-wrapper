﻿using System;
using System.Configuration;
using System.Globalization;
using System.Threading;
using SimpleConfigurationWrapper.Attributes;
using SimpleConfigurationWrapper.Providers;

namespace SimpleConfigurationWrapper.Entries
{

    /// <summary>
    /// The basic primitive types.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class PrimitiveEntry<T> : ConfigurationEntry<T> where T : IConvertible
    {
        private readonly CultureInfo _cultureInfo;

        public PrimitiveEntry(IStringConfigValueFacade valueFacade, string identifier, T defaultValue, string description = null, string examples = null, CultureInfo cultureInfo = null) : base(valueFacade, identifier, defaultValue, description, examples)
        {
            _cultureInfo = cultureInfo ?? CultureInfo.InvariantCulture;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stringConfigStoreFacade"></param>
        /// <param name="identifier"></param>
        /// <param name="defaultValue"></param>
        /// <param name="description"></param>
        /// <param name="examples"></param>
        /// <param name="cultureInfo"></param>
        public PrimitiveEntry(string identifier, T defaultValue, string description = null, string examples = null, CultureInfo cultureInfo = null) : base(identifier, defaultValue, description, examples)
        {
            _cultureInfo = cultureInfo ?? CultureInfo.InvariantCulture;
        }

        public override T ConvertFromString(string stringValue)
        {
            try
            {
                var result = ConvertToT(stringValue);
                return result;

            }
            catch (Exception ex)
            {
                throw new ConfigurationErrorsException("Encountered exception parsing configuration value " + stringValue + " for type " + typeof(T).Name + ". With message " + ex.Message, ex);
            }
        }

        private T ConvertToT(string stringValue)
        {
            if (typeof(T).IsEnum)
            {
                return (T)Enum.Parse(typeof(T), stringValue, true);
            }
            return (T)Convert.ChangeType(stringValue, typeof(T), _cultureInfo);
        }
        public override string ConvertToString(T value)
        {
            if (typeof(IConvertible).IsAssignableFrom(typeof(T)))
            {
                return ((IConvertible) value).ToString(_cultureInfo);
            }
            return value.ToString(_cultureInfo);
        }
    }
}