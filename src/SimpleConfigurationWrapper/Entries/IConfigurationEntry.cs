﻿using System.Xml.Schema;
using SimpleConfigurationWrapper.Providers;

namespace SimpleConfigurationWrapper.Entries
{
    /// <summary>
    /// An interface for the configuration entries, where a strong typed value can be retrieved.
    /// </summary>
    /// <typeparam name="TValue"></typeparam>
    /// <typeparam name="TConfigFormatType"></typeparam>
    public interface IConfigurationEntry<out TValue, TConfigFormatType> : IConfigurationEntry
    {
        /// <summary>
        /// Returns the T value for this Entry.
        /// </summary>
        TValue Value { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="valueFacade"></param>
        void SetConfigValueFacade(IConfigValueFacade<TConfigFormatType> valueFacade); 
    }


    /// <summary>
    /// The base interface for configurationentry, containing the functionality not dependent on the type of the configurationentry.    
    /// </summary>
    public interface IConfigurationEntry
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="includeDefaultValueString"></param>
        /// <returns></returns>
        ConfigurationEntryValidationResult Validate(bool includeDefaultValueString = false);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="valueFacade"></param>
        void SetConfigValueFacade(IConfigValueFacade valueFacade);

    }
}