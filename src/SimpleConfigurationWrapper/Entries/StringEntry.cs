﻿using System;
using System.Text;
using System.Threading.Tasks;
using SimpleConfigurationWrapper.Attributes;
using SimpleConfigurationWrapper.Providers;


namespace SimpleConfigurationWrapper.Entries
{
    [ConfigurationEntryFormatDocumentation("A simple string configuration", "\"example\"")]
    public class StringEntry : ConfigurationEntry<string>
    {
        public StringEntry(string identifier, string defaultValue = null, string description = null, string examples = null)
            : base(identifier, defaultValue, description, examples)
        {

        }
        public StringEntry(IStringConfigValueFacade stringValueFacade, string identifier, string defaultValue = null, string description = null, string examples = null) 
            : base(stringValueFacade, identifier, defaultValue, description, examples)
        {

        }


        public override string ConvertFromString(string stringValue)
        {
            return stringValue; 
        }

        public override string ConvertToString(string value)
        {
            return value; 
        }
    }
}
