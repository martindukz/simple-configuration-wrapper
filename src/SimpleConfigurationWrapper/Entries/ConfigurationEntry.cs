﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;
using SimpleConfigurationWrapper.Providers;

namespace SimpleConfigurationWrapper.Entries
{
    /// <summary>
    /// Represents configuration entries with a string format as backend. 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class ConfigurationEntry<T> : IConfigurationEntry<T, string>
    {
        private IConfigValueFacade<string> _stringConfigStoreFacade;

        /// <summary>
        /// The identifier/key used to retrieve value.
        /// </summary>
        public string Identifier { get; }
        /// <summary>
        /// Optional default value (i.e. what to return if not found)
        /// </summary>
        public T DefaultValue { get; }
        /// <summary>
        /// A description of the semantics of the configuration entry.
        /// </summary>
        public string Description { get; }
        /// <summary>
        /// A string containing examples of possible values.
        /// </summary>
        public string Examples { get; }
        /// <summary>
        /// Indicates whether value should be hidden if validation fails. 
        /// </summary>
        protected virtual bool Hidden { get; } = false;
        /// <summary>
        /// Indicates whether the string value is cachable. I.e. the value which is deserialized to the T return value. 
        /// </summary>
        protected virtual bool UseCache{ get; } = true;
        /// <summary>
        /// Indicates whether the value is cachable. (e.g. false if it is an object with a lifecycle and dependencies. If lifecycle dependent, then value should be created each time. So not cached. )
        /// </summary>
        protected virtual bool CacheReturnValue { get; } = true; 
        /// <summary>
        /// Current cached value. Null until cached. 
        /// </summary>
        private CachedValue<T> CacheValue { get; set; } = null; 
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="stringConfigStoreFacade"></param>
        /// <param name="identifier"></param>
        /// <param name="defaultValue"></param>
        /// <param name="description"></param>
        /// <param name="examples"></param>
        protected ConfigurationEntry(IStringConfigValueFacade stringConfigStoreFacade, string identifier, T defaultValue, string description = null, string examples = null)
        {
            _stringConfigStoreFacade = stringConfigStoreFacade ?? new ConfigurationManagerValueFacade();
            Identifier = identifier;
            DefaultValue = defaultValue;
            Description = description;
            Examples = examples;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identifier"></param>
        /// <param name="defaultValue"></param>
        /// <param name="description"></param>
        /// <param name="examples"></param>
        protected ConfigurationEntry(string identifier, T defaultValue, string description = null, string examples = null)
            : this(null, identifier, defaultValue, description, examples)
        {
            
        }
        /// <summary>
        /// Retrieve the deserialized value for the configurationentry.
        /// </summary>
        public T Value
        {
            get {
                if (CacheValue != null)
                {
                    if (CacheValue.UseDefault)
                    {
                        return DefaultValue;
                    }
                    if (CacheReturnValue)
                    {
                        return CacheValue.Value;
                    }
                    return ConvertFromString(CacheValue.StringValue);
                }

                string stringValue;
                if (!_stringConfigStoreFacade.GetValue(Identifier, out stringValue))
                {
                    CacheValue = new CachedValue<T>(true);
                    return DefaultValue;
                }
                //Consider caching here. I.e. the first time it is used, it is assumed to not change and thus the resulting value can just be returned.
                var result = ConvertFromString(stringValue);
                CacheValue = new CachedValue<T>(stringValue);
                if (CacheReturnValue)
                {
                    CacheValue.Value = result; 
                }
                return result; 
            }
        }

        public void SetConfigValueFacade(IConfigValueFacade<string> valueFacade)
        {
            _stringConfigStoreFacade = valueFacade;
        }

        /// <summary>
        /// Implicitely converts configuration entry to T. 
        /// </summary>
        /// <param name="convertThis"></param>
        public static implicit operator T(ConfigurationEntry<T> convertThis)
        {
            return convertThis.Value; 
        }

        /// <summary>
        /// Converts a string value to T. Implemented in subclasses.
        /// </summary>
        /// <param name="stringValue"></param>
        /// <returns></returns>
        public abstract T ConvertFromString(string stringValue);
        /// <summary>
        /// Converts to provided T value to a string.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public abstract string ConvertToString(T value);
        /// <summary>
        /// Validates (and caches value - if applicable)
        /// </summary>
        /// <returns></returns>
        public ConfigurationEntryValidationResult Validate(bool includeDefaultValueString = false)
        {
            var result = new ConfigurationEntryValidationResult(Identifier, DefaultValue, Description, Examples);
            
            try
            {
                if (includeDefaultValueString)
                {
                    result.DefaultValueString = ConvertToString(DefaultValue);
                }
                var value = Value;
                if (!Hidden)
                {
                    result.ConfigEntryValue = RawConfigValue();
                }
                else
                {
                    result.ConfigEntryValue = "!!!Config value hidden!!!";
                }
            }
            catch (Exception ex)
            {
                result.ValidationFailed = true;
                result.Exception = ex;
                result.ValidationMessage = "Failed with: " + ex.Message; 
                result.EnrichWithFormatInfo(GetType());
            }
            return result; 
        }

        public void SetConfigValueFacade(IConfigValueFacade valueFacade)
        {
            this.SetConfigValueFacade((IConfigValueFacade<string>)valueFacade);
        }

        public string RawConfigValue()
        {
            return _stringConfigStoreFacade.GetValue(Identifier); 
        }

        public override string ToString()
        {
            return ConvertToString(Value);
        }

        private class CachedValue<T>
        {
            public CachedValue(string stringValue)
            {
                StringValue = stringValue;
            }

            public CachedValue(bool useDefault)
            {
                UseDefault = useDefault; 
            }

            public string StringValue { get;  }
            public bool UseDefault { get;  }
            public T Value { get; set; }
        }
    }
}
