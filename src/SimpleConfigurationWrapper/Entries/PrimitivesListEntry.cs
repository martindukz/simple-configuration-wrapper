﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using SimpleConfigurationWrapper.Attributes;
using SimpleConfigurationWrapper.Providers;

namespace SimpleConfigurationWrapper.Entries
{
    /// <summary>
    /// A list of basic primitives.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [ConfigurationEntryFormatDocumentation("A list of primitives of the type T. Seperater can be passed along, if not wanting the default ,")]
    public class PrimitivesListEntry<T> : ConfigurationEntry<List<T>> where T : IConvertible
    {
        private readonly char _seperator;
        private readonly CultureInfo _cultureInfo;
    
        public PrimitivesListEntry(string identifier, List<T> defaultValue, char seperator = ',', string description = null, string examples = null, CultureInfo cultureInfo = null) : this(null, identifier, defaultValue, seperator, description, examples, cultureInfo)
        {
        }
        public PrimitivesListEntry(IStringConfigValueFacade stringConfigStoreFacade, string identifier, List<T> defaultValue, char seperator = ',', string description = null, string examples = null, CultureInfo cultureInfo = null) : base(stringConfigStoreFacade, identifier, defaultValue, description, examples)
        {
            _seperator = seperator;
            _cultureInfo = cultureInfo ?? CultureInfo.InvariantCulture;
        }


        public override List<T> ConvertFromString(string stringValue)
        {
            try
            {
                var result = stringValue
                    .Split(_seperator)
                    .Select(ConvertToT)
                    .ToList();

                return result;
            }
            catch (Exception ex)
            {
                throw new ConfigurationErrorsException("Encountered exception parsing configuration value " + stringValue + " for list with type " + typeof(T).Name + ". With message " + ex.Message, ex);
            }
          
        }

        private T ConvertToT(string stringValue)
        {
            if (typeof(T).IsEnum)
            {
                return (T)Enum.Parse(typeof(T), stringValue, true);
            }
            return (T) Convert.ChangeType(stringValue, typeof(T), _cultureInfo);
        }

        public override string ConvertToString(List<T> value)
        {
            var asStrings = value.Select(AsString);
            return String.Join(_seperator.ToString(), asStrings);
        }

        private string AsString(T arg)
        {
            if (typeof(IConvertible).IsAssignableFrom(typeof(T)))
            {
                return ((IConvertible)arg).ToString(_cultureInfo);
            }
            return arg.ToString();
        }
    }
}