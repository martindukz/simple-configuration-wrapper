﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleConfigurationWrapper.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class ConfigurationEntryFormatDocumentationAttribute : Attribute
    {
        public string ConfigurationDescription { get; }
        public IEnumerable<string> Examples { get; }

        public ConfigurationEntryFormatDocumentationAttribute(string configurationDescription, params string[] examples)
        {
            ConfigurationDescription = configurationDescription;
            Examples = examples;
        }
    }
}
