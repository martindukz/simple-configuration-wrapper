﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleConfigurationWrapper.Entries;
using SimpleConfigurationWrapper.Examples.ExampleClasses;
using SimpleConfigurationWrapper.Providers;

namespace SimpleConfigurationWrapper.Examples
{
    public class ExampleConfiguration : ConfigurationWrapper
    {
        public ExampleConfiguration()
        {

        }

        private static string GetFullIdentifier(string identifier)
        {
            return TypePrefix<ExampleConfiguration>(identifier);
        }

        public ConfigurationEntry<string> SomeString { get; } = new StringEntry(GetFullIdentifier(nameof(SomeString)), "SomeDefaultValue", "A string used for something"); 
        public ConfigurationEntry<int> SomeInt { get; } = new PrimitiveEntry<int>(GetFullIdentifier(nameof(SomeInt)), 2, "An int used for retries or similar");
        public ConfigurationEntry<bool> SomeBool { get; } = new PrimitiveEntry<bool>(GetFullIdentifier(nameof(SomeBool)), true, "Enable/disable something");
        public ConfigurationEntry<double> SomeDouble { get; } = new PrimitiveEntry<double>(GetFullIdentifier(nameof(SomeDouble)), 1.2, "Some double value for some purpose");
        public ConfigurationEntry<TimeSpan> SomeTimeSpan { get; } = new TimeSpanEntry(GetFullIdentifier(nameof(SomeTimeSpan)), TimeSpan.FromMinutes(33), "Some timespan. E.g. retry time or timout");
        public DepInjEntry<ISomeInterface> SomeDependencyType { get; } = new DepInjEntry<ISomeInterface>("ImplementationOfSomeInterface", typeof(SomeImpl1), "Have a mapping for an interface to be used by a DI container.");
        public ConfigurationEntry<List<double>> SomeDoubleList { get; } = new PrimitivesListEntry<double>(GetFullIdentifier(nameof(SomeDoubleList)), new List<double>() { 1.2 , 33, 3.3, -1.8338}, description: "A list of doubles... for some reason");
        public ConfigurationEntry<List<SomeEnum>> SomeEnumList { get; } = new PrimitivesListEntry<SomeEnum>(GetFullIdentifier(nameof(SomeEnumList)), new List<SomeEnum>() { SomeEnum.OtherValue, SomeEnum.ThisValue}, description: "A list of enums for some filter.", examples: "OtherValue, ThisValue");


    }

    public enum SomeEnum
    {
        ThisValue, 
        OtherValue, 
        NotUsableValue
    }
}
