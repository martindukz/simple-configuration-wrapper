﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleConfigurationWrapper.Examples.ExampleClasses
{
    public interface ISomeInterface
    {
        string Message { get; }
    }

    public class SomeImpl1 : ISomeInterface
    {
        public string Message {
            get { return GetType().Name;  }
        }
    }
    public class SomeImpl2 : ISomeInterface
    {
        public string Message
        {
            get { return GetType().Name; }
        }
    }
}
