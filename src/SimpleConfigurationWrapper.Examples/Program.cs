﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Windsor;
using Castle.Windsor.Installer;
using SimpleConfigurationWrapper.Examples.ExampleClasses;
using SimpleConfigurationWrapper.Providers;
using SimpleConfigurationWrapper.Util;

namespace SimpleConfigurationWrapper.Examples
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = new WindsorContainer();

            // adds and configures all components using WindsorInstallers from executing assembly
            container.Install(FromAssembly.This());
            var exampleConfig = new ExampleConfiguration()
                //If alternative value facade is wanted, it can be set here. 
                                    .WithValueProvider(new ConfigurationManagerValueFacade());

            var validationResult = exampleConfig.Validate();
            if (validationResult.Any(vr => vr.ValidationFailed))
            {
                throw new ConfigurationErrorsException(string.Join(Environment.NewLine, validationResult.Where(vr => vr.ValidationFailed).Select(vr => vr.ValidationMessage)));
            }

            if (exampleConfig.SomeInt > 32) throw new Exception(); 

            Console.WriteLine("Configuration value read: " + exampleConfig.SomeTimeSpan.Value);
            // instantiate and configure root component and all its dependencies and their dependencies and...
            var result = container.Resolve<ISomeInterface>(); 
            Console.WriteLine("ISomeInterface resolved to: " + result.GetType().Name + " with config key: " + exampleConfig.SomeDependencyType.RawConfigValue());

            // clean up, application exits
            
            
            //Implicit conversion: 
            string x = exampleConfig.SomeString;

            int i = exampleConfig.SomeInt;
            Console.WriteLine("How to configure: key=\"" + exampleConfig.SomeInt.Identifier + "\" value=\"" + exampleConfig.SomeInt.ConvertToString(exampleConfig.SomeInt.DefaultValue) + "\"");
            Console.WriteLine("Current value read: " + exampleConfig.SomeInt);

            //TODO: 
            //List with primitives.
            //Json object entry. 
            //Instances. 
            //Date
            //Timespan
            //Version
            //List of types (e.g. for strategy pattern)
            Console.WriteLine("Generate the default configuration:");
            var forTheConfig = new GenerateDefaultConfiguration().GenerateForAppSettings(exampleConfig);
            Console.Write(forTheConfig);

            Console.In.Read();
            container.Dispose();
        }
    }
}
