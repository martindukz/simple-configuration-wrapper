using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using SimpleConfigurationWrapper.Providers;

namespace SimpleConfigurationWrapper.Examples
{
    public class ConfigurationInstaller : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Component.For<IStringConfigValueFacade, ConfigurationManagerValueFacade>().LifestyleTransient());
            container.Register(Component.For<ExampleConfiguration, ExampleConfiguration>().LifestyleSingleton());
            var configuration = container.Resolve<ExampleConfiguration>();

            container.Register(Component.For(configuration.SomeDependencyType.SourceType, configuration.SomeDependencyType)); 
        }
    }
}