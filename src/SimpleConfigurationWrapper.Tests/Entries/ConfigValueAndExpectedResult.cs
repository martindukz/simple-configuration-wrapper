﻿namespace SimpleConfigurationWrapper.Tests.Entries
{
    public class ConfigValueAndExpectedResult<T>
    {
        public string ConfigValue { get; }
        public T ExpectedResult { get; }

        public ConfigValueAndExpectedResult(string configValue, T expectedResult)
        {
            ConfigValue = configValue;
            ExpectedResult = expectedResult;
        }
    }
}