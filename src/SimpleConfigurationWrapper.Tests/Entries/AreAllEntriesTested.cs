﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SimpleConfigurationWrapper.Entries;

namespace SimpleConfigurationWrapper.Tests.Entries
{
    [TestFixture]
    public class AreAllEntriesTested
    {
        [Test]
        public void IdentifyMissingConfigurationEntryTests()
        {
            var allRelevantTests = GetAllConfigEntryTests();

            var configEntriesTested = GetAllTestedConfigEntries(allRelevantTests);
            //Add additional assemblies here for testing. 
            var allConfigurationEntryTypes = typeof (ConfigurationEntry<>).Assembly.GetTypes()
                .Where(t => typeof (IConfigurationEntry).IsAssignableFrom(t))
                .Where(t => !t.IsAbstract && t.IsClass)
                .ToList();

            //Test nonGeneric: 
            var nonGenericConfigEntries = allConfigurationEntryTypes.Where(t => !t.IsGenericType).ToList();
            var nonGenericNotTested = nonGenericConfigEntries.Where(nonGenericConfigEntry => !configEntriesTested.Any(tested => tested == nonGenericConfigEntry)).ToList();

            var genericConfigEntries = allConfigurationEntryTypes.Where(t => t.IsGenericType).ToList();
            var genericNotTested = genericConfigEntries.Where(genericConfigEntry => !configEntriesTested.Where(t => t.IsGenericType).Any(tested => tested.GetGenericTypeDefinition() == genericConfigEntry.GetGenericTypeDefinition())).ToList();

            var allNotTested = nonGenericNotTested.ToList();
            allNotTested.AddRange(genericNotTested);

            Assert.IsEmpty(allNotTested, "Not tested: " + string.Join(",", allNotTested.Select(t => t.Name)));
        }

        [Test]
        public void EnsureAllTestsAreMarkedWithTestFixture()
        {
            var allRelevantTests = GetAllConfigEntryTests();

            Assert.IsFalse(allRelevantTests.Any(t => !t.GetCustomAttributes<TestFixtureAttribute>().Any()));
        }

        private static List<Type> GetAllTestedConfigEntries(List<Type> allRelevantTests)
        {
            var allInterfaces = allRelevantTests.SelectMany(testType => testType.GetInterfaces()).ToList();

            var relevantInterfaces = allInterfaces
                .Where(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof (IConfigEntryTest<>)).ToList();

            var configEntriesTested = relevantInterfaces.Select(i => i.GetGenericArguments().First()).ToList();
            return configEntriesTested;
        }

        private List<Type> GetAllConfigEntryTests()
        {
            var allRelevantTests =
                this.GetType()
                    .Assembly.GetTypes()
                    .Where(t => (typeof (IConfigEntryTest)).IsAssignableFrom(t))
                    .Where(t => t.IsClass && !t.IsAbstract)
                    .ToList();
            return allRelevantTests;
        }

        [Test]
        public void IdentifyImplementedConfigurationEntryTest()
        {
            var type = typeof (DepInjEntryTests);
            var interfaces = type.GetInterfaces();
            var relevant = interfaces.Where(i => i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IConfigEntryTest<>))
                    .Select(i => i.GetGenericArguments().First()).ToList();

            Assert.AreEqual(1, relevant.Count);
            Assert.AreEqual(typeof(DepInjEntry<>), relevant.First().GetGenericTypeDefinition());


            //var relevant.First().GetInterfaces()
            //    .Where(i => i.IsGenericType)
            //    .Any(i => i.GetGenericTypeDefinition() == typeof(IConfigurationEntry<>));
        }
    }

}
