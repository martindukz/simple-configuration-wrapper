﻿using SimpleConfigurationWrapper.Entries;
using SimpleConfigurationWrapper.Providers;

namespace SimpleConfigurationWrapper.Tests.Entries
{
    public class StringEntryTests : ConfigurationEntryTestTemplate<StringEntry, string>
    {
        protected override bool TestForGibberish { get; } = false;

        protected override StringEntry CreateTestTargetEntry(IStringConfigValueFacade facade)
        {
            return new StringEntry(facade, "test", "defaultvalue");
        }

        protected override ConfigValueAndExpectedResult<string> SimpleTestReturnValue => new ConfigValueAndExpectedResult<string>("hest", "hest");
        protected override ConfigValueAndExpectedResult<string> AlternativeTestReturnValue => new ConfigValueAndExpectedResult<string>("\"\"horse", "\"\"horse");
    }
}