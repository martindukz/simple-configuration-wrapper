﻿using Moq;
using NUnit.Framework;
using SimpleConfigurationWrapper.Entries;
using SimpleConfigurationWrapper.Providers;

namespace SimpleConfigurationWrapper.Tests.Entries.ConfigurationEntry
{
    [TestFixture]
    public class CachingTests
    {
        [Test]
        public void StringValueOnlyRetrievedTheFirstTime()
        {

            var stringValueFacade = new StringValueStub("returned");
            var configEntry = new TestEntry(stringValueFacade, "xx");//new TestEntry(mock.Object, "x");
            Assert.AreEqual("returned", configEntry.Value);
            Assert.AreEqual("returned", configEntry.Value);

            Assert.AreEqual(1, configEntry.ConvertFromStringCount);
        }

        [Test]
        public void StringFacadeOnlyCalledOnce()
        {
            var stringValueFacade = new StringValueStub("returned");
            var configEntry = new TestEntry(stringValueFacade, "xx");//new TestEntry(mock.Object, "x");
            Assert.AreEqual("returned", configEntry.Value);
            Assert.AreEqual("returned", configEntry.Value);

           Assert.AreEqual(1, stringValueFacade.InvocationCounts);
        }
        
    }

    public class TestEntry : ConfigurationEntry<string>
    {
        public int ConvertFromStringCount = 0;
     
        public TestEntry(IStringConfigValueFacade stringConfigStoreFacade, string identifier, string defaultValue = null, string description = null, string examples = null) : base(stringConfigStoreFacade, identifier, defaultValue, description, examples)
        {
        }

        public TestEntry(string identifier, string defaultValue, string description = null, string examples = null) : base(identifier, defaultValue, description, examples)
        {
        }

        public override string ConvertFromString(string stringValue)
        {
            ConvertFromStringCount++;
            return stringValue;
        }

        public override string ConvertToString(string value)
        {
            return value;
        }
    }
}
