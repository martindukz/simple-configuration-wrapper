﻿using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SimpleConfigurationWrapper.Entries;
using SimpleConfigurationWrapper.Providers;

namespace SimpleConfigurationWrapper.Tests.Entries
{
    [TestFixture]
    public abstract class ConfigurationEntryTestTemplate<TEntry, TValue> : IConfigEntryTest<TEntry, TValue> where TEntry : IConfigurationEntry<TValue, string>
    {
        protected abstract TEntry CreateTestTargetEntry(IStringConfigValueFacade facade);
        protected abstract ConfigValueAndExpectedResult<TValue> SimpleTestReturnValue { get; }
        protected abstract ConfigValueAndExpectedResult<TValue> AlternativeTestReturnValue { get; }
        protected virtual bool ReturnValueCanEqualityCompare { get; } = true;
        protected virtual string Gibberish { get; } = ":fpdjsabvupd8943ugb8phesnje ipoh igrepoih rwpi g348u rehbjnfdsel brhuw iphburiep";
        protected virtual bool TestForGibberish { get; } = true;

        protected virtual void AreEqual(TValue expected, TValue actual)
        {
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void ValidateGibberishThrows()
        {
            if (!TestForGibberish)
            {
                return;
            }
            var target = CreateTestTargetEntry(new StringValueStub(Gibberish));

            Assert.Throws<ConfigurationErrorsException>(() => { var result = target.Value; });
        }

        [Test]
        public void ValidateSimpleConfigValue()
        {

            var target = CreateTestTargetEntry(new StringValueStub(SimpleTestReturnValue.ConfigValue));

            AreEqual(SimpleTestReturnValue.ExpectedResult, target.Value);
        }

        [Test]
        public void ValidateAlternativeConfigValue()
        {

            var target = CreateTestTargetEntry(new StringValueStub(AlternativeTestReturnValue.ConfigValue));

            AreEqual(AlternativeTestReturnValue.ExpectedResult, target.Value);
        }
    }
}
