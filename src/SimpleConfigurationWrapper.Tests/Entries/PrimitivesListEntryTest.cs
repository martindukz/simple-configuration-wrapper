﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using SimpleConfigurationWrapper.Entries;
using SimpleConfigurationWrapper.Providers;

namespace SimpleConfigurationWrapper.Tests.Entries
{
    public class PrimitivesListEntryTest : ConfigurationEntryTestTemplate<PrimitivesListEntry<int>, List<int>>
    {

        protected override PrimitivesListEntry<int> CreateTestTargetEntry(IStringConfigValueFacade facade)
        {
           return new PrimitivesListEntry<int>(facade, "test", new List<int>() { 41});
        }

        protected override void AreEqual(List<int> expected, List<int> actual)
        {
            CollectionAssert.AreEqual(expected, actual);
        }

        protected override ConfigValueAndExpectedResult<List<int>> SimpleTestReturnValue => new ConfigValueAndExpectedResult<List<int>>("1", new List<int>() {1});
        protected override ConfigValueAndExpectedResult<List<int>> AlternativeTestReturnValue => new ConfigValueAndExpectedResult<List<int>>("1,2,3,4,4", new List<int>() { 1,2,3,4,4 });
    }
}