﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SimpleConfigurationWrapper.Entries;
using SimpleConfigurationWrapper.Extensions;
using SimpleConfigurationWrapper.Providers;

namespace SimpleConfigurationWrapper.Tests.Entries
{
    [TestFixture]
    public class DictionaryEntryTests : ConfigurationEntryTestTemplate<DictionaryEntry<string, string>, Dictionary<string, string>>
    {
        protected override ConfigValueAndExpectedResult<Dictionary<string,string>> AlternativeTestReturnValue =>
            new ConfigValueAndExpectedResult<Dictionary<string,string>>("x:y;z:1;empt:", new Dictionary<string,string>() { {"x", "y"}, {"z", "1"}, {"empt", string.Empty} });

        protected override ConfigValueAndExpectedResult<Dictionary<string, string>> SimpleTestReturnValue =>
            new ConfigValueAndExpectedResult<Dictionary<string, string>>("xcds:ysss", new Dictionary<string, string>() { { "xcds", "ysss" }});

        protected override void AreEqual(Dictionary<string, string> expected, Dictionary<string, string> actual)
        {
            CollectionAssert.AreEquivalent(expected.Select(kvp => kvp.Key +":" + kvp.Value).ToList(), actual.Select(kvp => kvp.Key + ":" + kvp.Value).ToList());
        }

   
        protected override DictionaryEntry<string, string> CreateTestTargetEntry(IStringConfigValueFacade facade)
        {
            return new DictionaryEntry<string, string>(facade, "test", new Dictionary<string, string>());
        }
    }
}
