using System;
using NUnit.Framework;
using SimpleConfigurationWrapper.Entries;
using SimpleConfigurationWrapper.Providers;

namespace SimpleConfigurationWrapper.Tests.Entries
{
    [TestFixture]
    public class DepInjEntryTests : ConfigurationEntryTestTemplate<DepInjEntry<DepInjEntryTests.IDepInjTestSource>, Type>
    {

        protected override DepInjEntry<IDepInjTestSource> CreateTestTargetEntry(IStringConfigValueFacade facade)
        {
            return new DepInjEntry<IDepInjTestSource>(facade, "test", typeof(DefaultTarget));
        }

        protected override ConfigValueAndExpectedResult<Type> SimpleTestReturnValue { get; } = new ConfigValueAndExpectedResult<Type>(typeof(Target).AssemblyQualifiedName, typeof(Target));
        protected override ConfigValueAndExpectedResult<Type> AlternativeTestReturnValue { get; } = new ConfigValueAndExpectedResult<Type>(typeof(Target).FullName + ", " + typeof(Target).Assembly.FullName, typeof(Target));
        
        public interface IDepInjTestSource
        {
        }

        public class Target
        {

        }



        public class DefaultTarget
        {

        }


    }
}