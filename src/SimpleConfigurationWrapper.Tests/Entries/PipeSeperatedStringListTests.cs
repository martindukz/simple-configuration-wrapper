﻿using System.Collections.Generic;
using NUnit.Framework;
using SimpleConfigurationWrapper.Entries;
using SimpleConfigurationWrapper.Providers;

namespace SimpleConfigurationWrapper.Tests.Entries
{
    [TestFixture]
    public class PipeSeperatedStringListTests : ConfigurationEntryTestTemplate<PipeSeperatedStringList, List<string>>
    {
        protected override bool TestForGibberish { get; } = false; 

        protected override PipeSeperatedStringList CreateTestTargetEntry(IStringConfigValueFacade facade)
        {
            return new PipeSeperatedStringList(facade, "test", new List<string>());
        }

        protected override ConfigValueAndExpectedResult<List<string>> SimpleTestReturnValue => new ConfigValueAndExpectedResult<List<string>>("xx", new List<string>() { "xx"});
        protected override ConfigValueAndExpectedResult<List<string>> AlternativeTestReturnValue => new ConfigValueAndExpectedResult<List<string>>("xx|yy|zz", new List<string>() { "xx", "yy", "zz" });

        protected override void AreEqual(List<string> expected, List<string> actual)
        {
            CollectionAssert.AreEqual(expected, actual);
        }

        [Test]
        public void ValidateAlternativeSeperator()
        {
            var target = new PipeSeperatedStringList(new StringValueStub("x,y,z"), "test", seperator: ',');
            CollectionAssert.AreEquivalent(new List<string>() {"x", "y", "z"}, target.Value);
        }
    }
}