namespace SimpleConfigurationWrapper.Tests.Entries
{
    public interface IConfigEntryTest<TEntry, TValue> : IConfigEntryTest<TEntry>
    {

    }

    public interface IConfigEntryTest<TEntry> : IConfigEntryTest
    {
        
    }

    public interface IConfigEntryTest
    {
    }
}