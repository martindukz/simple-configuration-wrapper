﻿using SimpleConfigurationWrapper.Entries;
using SimpleConfigurationWrapper.Providers;

namespace SimpleConfigurationWrapper.Tests.Entries
{
    public class PrimitiveEntryTests : ConfigurationEntryTestTemplate<PrimitiveEntry<double>, double>
    {
        protected override bool TestForGibberish { get; } = false;

        protected override PrimitiveEntry<double> CreateTestTargetEntry(IStringConfigValueFacade facade)
        {
            return new PrimitiveEntry<double>(facade, "test", 32.4);
        }

        protected override ConfigValueAndExpectedResult<double> SimpleTestReturnValue => new ConfigValueAndExpectedResult<double>("1.2", 1.2);
        protected override ConfigValueAndExpectedResult<double> AlternativeTestReturnValue => new ConfigValueAndExpectedResult<double>("43", 43);
    }
}