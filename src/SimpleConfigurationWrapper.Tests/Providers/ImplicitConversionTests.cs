﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using NUnit.Framework;
using SimpleConfigurationWrapper.Entries;
using SimpleConfigurationWrapper.Providers;

namespace SimpleConfigurationWrapper.Tests.Providers
{
    [TestFixture]
    public class ImplicitConversionTests
    {
        [Test]
        public void TestImplicitConversion()
        {
            var mock = new Mock<IStringConfigValueFacade>();
            var outVar = "xyz";
            mock.Setup(con => con.GetValue(It.IsAny<string>(), out outVar)).Returns(true);

            var stringEntry = new StringEntry(mock.Object, "key", "x");
            string asString = stringEntry; 
            Assert.AreEqual("xyz", asString);
        }
        [Test]
        public void TestImplicitConversion_noEntry()
        {
            var mock = new Mock<IStringConfigValueFacade>();
            var outVar = "";
            mock.Setup(con => con.GetValue(It.IsAny<string>(), out outVar)).Returns(false);

            var stringEntry = new StringEntry(mock.Object, "key", "x");
            string asString = stringEntry;
            Assert.AreEqual("x", asString);
        }
    }
}
