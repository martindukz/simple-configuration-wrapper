﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace SimpleConfigurationWrapper.Tests.Providers
{
    [TestFixture]
    public class ConfigurationManagerValueFacadeTests
    {
        [Test]
        public void AppConfig_IdentifyKeyNotFound()
        {
            var hasKey = ConfigurationManager.AppSettings.AllKeys.Contains("someNonExistingKey");
            Assert.IsFalse(hasKey);
        }

        [Test]
        public void AppConfig_EmptyStringReturnedIfValueNotDefined()
        {
            var retrieved = ConfigurationManager.AppSettings["ValueNotDefined"]; 
            Assert.AreEqual("", retrieved);
        }

        [Test]
        public void AppConfig_IdentifyKeyWithEmptyStringValue()
        {
            var retrieved = ConfigurationManager.AppSettings["ValueIsEmptyString"];
            Assert.AreEqual("", retrieved);
        }
    }
}
