﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleConfigurationWrapper.Extensions;
using SimpleConfigurationWrapper.Providers;

namespace SimpleConfigurationWrapper.Tests
{
    public class StringValueStub : IStringConfigValueFacade
    {
        private readonly string _returnThis;
        private readonly bool _hasValue;

        public StringValueStub(string returnThis, bool hasValue = true)
        {
            _returnThis = returnThis;
            _hasValue = hasValue;
        }

        public int InvocationCounts { get; set; } = 0; 

        public bool GetValue(string identifier, out string result)
        {
            InvocationCounts++; 
            result = _returnThis; 
            if (!_hasValue)
            {
                return false; 
            }
            return true; 
        }

        public string GetValue(string identifier, bool throwIfNotFound = false)
        {
            InvocationCounts++;
            return _returnThis;
        }
    }
}
